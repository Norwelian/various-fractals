﻿# Fractal code:

This program shows various implementations of some fractals.

It uses a little "launcher" I made to ease the launch of the code.

Simply execute "launcher.py" with python, and you are good to go!

If you want to read or change the code, there is a "config.conf" file in every fractal folder. This file holds the different parameters the fractal in question is gonna use.

If you want to run a fractal on its own, without the launcher, simply: `python code.py param1 param2....`

# Implemented fractals:

* Pythagorean Tree
* Cantor Set
* Koch Snowflake
* Koch Random Snowflake
* Koch Curve
* Hilbert Curve
* Pascal's Triangle