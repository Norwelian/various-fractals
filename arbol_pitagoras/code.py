import matplotlib.pyplot as plt
import numpy as np
import sys

#sys.path.insert(0, '/home/norwelian/Documents/UPM/Fractales/Codigo_Fractales/box_counting/')

#import code as co

a = float(sys.argv[2])
seg = [0.,1.,0.,0.]

angle = 0

def giro(datax1, datax2, datay1, datay2, angulo):
        grade = np.deg2rad(angulo)
        return np.array( ((1,0,datax1),(0,1,datay1),(0,0,1)) ).dot( np.array( ((np.cos(grade), -np.sin(grade), 0), (np.sin(grade), np.cos(grade), 0), (0,0,1)) ) ).dot( np.array( ((1,0,-datax1), (0,1,-datay1), (0,0,1)) ) ).dot( np.array( ((datax2), (datay2), (1)) ) )

def homotecia(point, center, k):
        return [(point[0]-center[0])*k + center[0], (point[1]-center[1])*k + center[1]]

def iter(seg_base):
        global angle
        [x1,x2,y1,y2] = seg_base
        [x3,y3,z3] = giro(x2,x1,y2,y1,-90)
        [x4,y4,z4] = giro(x3,x2,y3,y2,-90)
        cuadradox = [x1,x2,x3,x4,x1]
        cuadradoy = [y1,y2,y3,y4,y1]
        l = np.sqrt((x3-x4)**2 + (y3-y4)**2)
        h = np.sqrt((a*l)*((1.-a)*l))
        la = a*l
        hipo = np.sqrt(h*h + la*la)
        [vectx,vecty] = homotecia([x3,y3],[x4,y4],(hipo/l))
        if angle == 0:
                angle = np.rad2deg(np.arctan(h/float(la)))
        triag1x = [x4,giro(x4,vectx,y4,vecty,angle)[0],x3]
        triag1y = [y4,giro(x4,vectx,y4,vecty,angle)[1],y3]
        #print "Angle: ", angle, "h: ", h, "la: ", la, "hipo: ", hipo, "vect: [", vectx, ", ", vecty, "]", "triag: [", triag1x[1], ", ", triag1y[1], "]"
        [newx, newy] = [cuadradox+triag1x, cuadradoy+triag1y]
        plt.plot(newx,newy,color='red')
        return [[triag1x[1],x3,triag1y[1],y3],[x4,triag1x[1],y4,triag1y[1]]]

def paint_two(seg1,seg2,n):
        iter(seg1)
        iter(seg2)
#        print "N: ", n

def iters(n,seg_base):
        [seg1,seg2] = iter(seg_base)
        if 0 < n-1:
                paint_two(seg1,seg2,n)
                iters(n-1,seg1)
                iters(n-1,seg2)

iters(int(sys.argv[1]),seg)
plt.show()
        
