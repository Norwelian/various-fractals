import matplotlib.pyplot as plt
import sys

data0 = [0.,1.]
y = 1
r = 1./3.

#print "Original data: ", data0

def iter(data, yX):
    global y
    newdata = []
    i = 0
    while i < len(data):
        data1 = [data[i],data[i]+r**y,data[i+1]-r**y,data[i+1]]
        i = i+2
        newdata = newdata + data1
    #print "Iteration ", y, " data: ", newdata
    plotCantor(newdata, yX)
    y = y+1
    return newdata

def iters(data, num_iter):
    i = 0.
    while i < num_iter:
        data = iter(data,i)
        i=i+1.
    return data

def plotCantor(data, yX):
    i = 0
    #print "Plotting: ", data
    while i < (len(data)/2):
        plt.plot([data[i],data[i+1]], [yX/6,yX/6], color="black")
        i=i+2
        
iters(data0,int(sys.argv[1]))
plt.axis([-0.1,0.4,-0.1,2.1])
#plt.savefig('Cantor.png', dpi=2000)
plt.show()
