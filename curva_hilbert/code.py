import sys
import matplotlib.pyplot as plt
import numpy as np

data0x = [1./4., 1./4., 3./4., 3./4.]
data0y = [1./4., 3./4., 3./4., 1./4.]
punto_traslacion = 3.

def giro(datax1, datax2, datay1, datay2, angulo):
    grade = np.deg2rad(angulo)
    return np.array( ((1,0,datax1),(0,1,datay1),(0,0,1)) ).dot( np.array( ((np.cos(grade), -np.sin(grade), 0), (np.sin(grade), np.cos(grade), 0), (0,0,1)) ) ).dot( np.array( ((1,0,-datax1), (0,1,-datay1), (0,0,1)) ) ).dot( np.array( ((datax2), (datay2), (1)) ) )

def traslacion(point, vect):
    return [(point[0] + vect[0]), (point[1] + vect[1])]

def homotecia(point, center, k):
    return [(point[0]-center[0])*k + center[0], (point[1]-center[1])*k + center[1]]
    
def procfiguraR(datax, datay):
    newdatax = []
    newdatay = []
    i = 0
    vector = []
    while i < len(datax):
        [data1x, data1y, _] = giro(1./2., datax[i], 1./2., datay[i], 90)
        [data1x, data1y] = homotecia([data1x, data1y], [1./2.,1./2.], 0.5)
        if i == 0:
            pointy = 1./(2**punto_traslacion)
            pointx = ((2**punto_traslacion)-1)*pointy
            vector = [pointx - data1x, pointy - data1y]
        [data1x, data1y] = traslacion([data1x, data1y], vector)
        [newdatax, newdatay] = [newdatax + [data1x], newdatay + [data1y]]
        i=i+1
    return [list(reversed(newdatax)), list(reversed(newdatay))]

def procfiguraL(datax, datay):
    newdatax = []
    newdatay = []
    i = 0
    vector = []
    datax = list(reversed(datax))
    datay = list(reversed(datay))
    while i < len(datax):
        [data1x, data1y, _] = giro(1./2., datax[i], 1./2., datay[i], 360-90)
        [data1x, data1y] = homotecia([data1x, data1y], [1./2.,1./2.], 0.5)
        if i == 0:
            pointx = 1./(2**punto_traslacion)
            pointy = pointx
            vector = [pointx - data1x, pointy - data1y]
        [data1x, data1y] = traslacion([data1x, data1y], vector)
        [newdatax, newdatay] = [newdatax + [data1x], newdatay + [data1y]]
        i=i+1
    
    return [newdatax, newdatay]

def procfiguraTL(datax, datay):
    newdatax = []
    newdatay = []
    i = 0
    vector = []
    while i < len(datax):
        [data1x, data1y] = homotecia([datax[i], datay[i]], [1./2.,1./2.], 0.5)
        if i == 0:
            pointx = 1./(2**punto_traslacion)
            pointy = (((2**punto_traslacion)/2.)+1)*pointx
            vector = [pointx - data1x, pointy - data1y]
        [data1x, data1y] = traslacion([data1x, data1y], vector)
        [newdatax, newdatay] = [newdatax + [data1x], newdatay + [data1y]]
        i=i+1
    return [newdatax, newdatay]

def procfiguraTR(datax, datay):
    newdatax = []
    newdatay = []
    i = 0
    vector = []
    while i < len(datax):
        [data1x, data1y] = homotecia([datax[i], datay[i]], [1./2.,1./2.], 0.5)
        if i == 0:
            pointx = (((2**punto_traslacion)/2.)+1)/(2**punto_traslacion)
            vector = [pointx - data1x, pointx - data1y]
        [data1x, data1y] = traslacion([data1x, data1y], vector)
        [newdatax, newdatay] = [newdatax + [data1x], newdatay + [data1y]]
        i=i+1
    return [newdatax, newdatay]


def iter(datax, datay):
    global punto_traslacion
    newdatax = []
    newdatay = []
    figura_girada_R = procfiguraR(datax, datay)
    figura_girada_L = procfiguraL(datax, datay)
    figura_TL = procfiguraTL(datax, datay)
    figura_TR = procfiguraTR(datax, datay)
    [newdatax, newdatay] = [figura_girada_L[0] + figura_TL[0] + figura_TR[0] + figura_girada_R[0], figura_girada_L[1] + figura_TL[1] + figura_TR[1] + figura_girada_R[1]]
    punto_traslacion = punto_traslacion + 1    
    return [newdatax, newdatay]

def iters(datax, datay, num_iters):
    i = 0
    while i < num_iters:
        [datax, datay] = iter(datax, datay)
        i = i+1
    return [datax, datay]

#plt.plot(data0x, data0y, 'ro')
[data0x, data0y] = iters(data0x, data0y, int(sys.argv[1]))
plt.plot(data0x, data0y, color="red", linewidth = 2)
"""
#1 box
plt.plot([1,0],[1,1], color = "black")
plt.plot([1,0],[0,0], color = "black")
plt.plot([1,1],[1,0], color = "black")
plt.plot([0,0],[1,0], color = "black")
#1/2 cross
plt.plot([1./2., 1./2.], [1., 0.], color = "black")
plt.plot([1,0], [1./2., 1./2.], color = "black")
#1/4 cross
plt.plot([1./4., 1./4.], [0,1], color = "green")
plt.plot([3./4., 3./4.], [0,1], color = "green")
plt.plot([0,1], [1./4., 1./4.], color = "green")
plt.plot([0,1], [3./4., 3./4.], color = "green")
"""
plt.axis([-0.1,1.1,-0.1,1.1])
plt.show()
