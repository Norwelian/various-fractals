import sys
import matplotlib.pyplot as plt
import numpy as np
import random as rd

data0x = [0.,1.]
data0y = [0.,0.]
r = float(sys.argv[2])
y = 1

def giro60(datax1, datax2, datay1, datay2):
    grade = np.pi/3
    return np.array( ((1,0,datax1),(0,1,datay1),(0,0,1)) ).dot( np.array( ((np.cos(grade), -np.sin(grade), 0), (np.sin(grade), np.cos(grade), 0), (0,0,1)) ) ).dot( np.array( ((1,0,-datax1), (0,1,-datay1), (0,0,1)) ) ).dot( np.array( ((datax2), (datay2), (1)) ) )

def giro300(datax1, datax2, datay1, datay2):
    grade = -np.pi/3
    return np.array( ((1,0,datax1),(0,1,datay1),(0,0,1)) ).dot( np.array( ((np.cos(grade), -np.sin(grade), 0), (np.sin(grade), np.cos(grade), 0), (0,0,1)) ) ).dot( np.array( ((1,0,-datax1), (0,1,-datay1), (0,0,1)) ) ).dot( np.array( ((datax2), (datay2), (1)) ) )

        
    
def iter(datax, datay):
    global y
    newdatax = []
    newdatay = []
    i = 0
    while i < (len(datax)-1):
        vector = np.array( ((datax[i+1]-datax[i]),(datay[i+1]-datay[i])) )
        tam_enterox = np.abs(datax[i+1]-datax[i])
        tam_entero = np.linalg.norm( vector )
        tam_segmento = tam_entero-(2*(r**y))
        centro_segmento = tam_segmento / 2
        datax3 = datax[i] + vector[0]*r
        datax4 = datax[i+1] - vector[0]*r
        datay3 = datay[i] + vector[1]*r
        datay4 = datay[i+1] - vector[1]*r
        ydiff = np.abs(datay[i] - datay[i+1])
        if rd.randint(0,100) < 50:
            [giro60x, giro60y, giro60z] = giro300(datax3, datax4, datay3, datay4)
        else:
            [giro60x, giro60y, giro60z] = giro60(datax3, datax4, datay3, datay4)
        data1x = [datax[i], datax3, giro60x, datax4, datax[i+1]]
        data1y = [datay[i], datay3, giro60y, datay4, datay[i+1]]
        [newdatax, newdatay] = [newdatax + data1x, newdatay + data1y]
        #print "i: ", i, "\nDataX: ", data1x, "\nDataY: ", data1y, "\n\nTam_segmento: ", tam_segmento, "\nTam_entero: ", tam_entero, "\nVector: ", vector
        i=i+1
    y = y+1
    return [newdatax, newdatay]

def iters(datax, datay, num_iters):
    i = 0
    while i < num_iters:
        [datax, datay] = iter(datax, datay)
        i = i+1
    return [datax, datay]

[data0x, data0y] = iters(data0x, data0y, int(sys.argv[1]))
plt.plot(data0x, data0y, color="black")
plt.axis([-0.1,1.1,-0.1,0.51])
plt.show()
