import os
import re

dir_frac = "./"

answers=[]

FRACTION_REGEX = re.compile(r'^(\d+)(?:(?:\s+(\d+))?/(\d+))?$')

def parseF(x):
      i, n, d = FRACTION_REGEX.match(x).groups()
      if d is None: return i  # if d is None, then n is also None
      if n is None: i, n = 0, i
      return float(i) + float(n) / float(d)

def get_fracs(DIR):
    i = 0
    L = []
    for filename in os.listdir(os.path.expanduser(DIR)):
        if "." not in filename:
            print(repr(i) + ". " + str(filename))
            i = i+1
            L = L + [filename]
    return L


def ask_frac_num(fracs,DIR):
    n = int(raw_input("> "))
    if n >= 0 and n < len(fracs):
        return os.path.expanduser(DIR + "/" + fracs[n])

def get_config(DIR):
    f=open(DIR + "/config.conf","r")
    for i in f:
        exec(i)
        nombre_var = i[0:i.find("=")]
        globals()[str(nombre_var)]=locals()[str(nombre_var)]
    
list_frac = get_fracs(dir_frac)
frac_dir = ask_frac_num(list_frac, dir_frac)
get_config(frac_dir)
print("Seccionado " + '"' + name + '"')

for i in params:
    temp = raw_input(i[0] + " (" + type(i[1]).__name__ + ": " + '%.3f' % i[1] + "): ")
    if temp:
        answers=answers+[str(parseF(temp))]
    else:
        answers=answers+[str(i[1])]

        
os.execlp("python", "python", frac_dir + "/code.py", *answers)
