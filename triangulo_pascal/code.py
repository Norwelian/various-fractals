import sys
import matplotlib.pyplot as plt
import numpy as np

points = [[1],[1,1]]

def iter(P,n,m):
    newlist = [1]
    j=1
    while(j<len(P[n-1])):
        newlist = newlist + [(P[n-1][j-1] + P[n-1][j])%m]
        j = j+1
    newlist = newlist + [1]
    P = P + [newlist]
    return P

def iters(P,n,m):
    i = 0
    while i < (n-2):
        P = iter(P,i+2,m)
        i = i+1
    return P

def plotList(P1,P2,c1,c2):
    i = 0
    j = 0
    diff = 1
    while i < len(P1):
        while j < (i+1):
            modmitad = (i+1)%2 #If = 1, centered
            mitad = (i+1)/2
            colr = [0.,0.,0.]
            if(P1[i][j] != 0):
                colr = [x+y for x,y in zip(colr, c1)]
            else:
                colr = [x+y for x,y in zip(colr, [0,0,0.5])]    
            if(P2[i][j] != 0):
                colr = [x+y for x,y in zip(colr, c2)]
            else:
                colr = [x+y for x,y in zip(colr, [0,0,0.5])]
            
                
            if(modmitad == 1):
                plt.plot((mitad-j)*(-diff), -i, color = tuple(colr), marker='o', markersize = 10)
            else:
                plt.plot((mitad-j)*(-diff) + 0.5*diff, -i, color = tuple(colr), marker='o', markersize = 10)
                
            j=j+1
        j=0
        i=i+1

points1 = iters(points,int(sys.argv[1]),int(sys.argv[2]))
#points2 = iters(points,3**4,)
plotList(points1,points1,[1.,0.,0.],[0.,1.,0.])
plt.show()


